<?php

use App\Http\Livewire\Alpine;
use App\Http\Livewire\Counter;
use App\Http\Livewire\Formulario;
use App\Http\Livewire\ListaUsuarios;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});

Route::get('/counter', Counter::class);

Route::get('/formulario', Formulario::class);

Route::get('/lista', ListaUsuarios::class);

Route::get('/users', function(){
    return view('users');
});

Route::get('/alpine',Alpine::class);