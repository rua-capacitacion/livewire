<div class="p-12">
    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    <form class="flex p-12 shadow-xl bg-gray-50 dark:bg-gray-700 dark:text-gray-400 rounded-2xl" wire:submit.prevent="submit">
        <div class="w-full lg:w-1/3">
            <label class="p-2">
                Nombre:
                <input class="rounded-2xl" type="text" wire:model.lazy="name" placeholder="Juan Pérez" />
            </label>
            @error('name') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="w-full lg:w-1/3">
            <label class="p-2">
                Correo:
                <input class="rounded-2xl" type="email" wire:model.lazy="email" placeholder="juan@unam.mx" />
            </label>
            @error('email') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="w-full lg:w-1/3">
            <label class="p-2">
                Contraseña:
                <input class="rounded-2xl" type="password" wire:model.lazy="password"  />
            </label>
            @error('password') <span class="error">{{ $message }}</span> @enderror
        </div>
        <div class="w-full pt-10 text-center">
            <button class="pt-2 pb-2 pl-10 pr-10 text-white bg-blue-600 rounded-2xl" type="submit">Guardar</button>
            <button class="pt-2 pb-2 pl-10 pr-10 text-white bg-blue-600 rounded-2xl" type="button" wire:click="limpiar">Limpiar</button>
        </div>
    </form>
</div>
