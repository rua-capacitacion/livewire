<div>
    <p>Counter desde livewire {{ $count }}</p>

    <div x-data="{ocount:@entagle('count'), nombre:null}" @obtener-nombre.window="nombre = $event.detail.nombre">
        <p x-text="`Count en alpine ${ocount}`"></p>
        <p x-text="`Nombre: ${nombre}`"></p>
        <button @click="$wire.increment()">+ (alpine)</button>
        <button @click="$wire.decrement()">- (alpine)</button>
        <p x-text=""></p>
    </div>

    <button wire:click="increment">+</button>
    <button wire:click="decrement">-</button>
    <button wire:click="emitirMensaje">Obtener nombre</button>
    <button wire:click="emitir">Mensaje</button>

    <script>
        document.addEventListener('livewire:load', function(){
            Livewire.on('usuario-add', result => {
                console.log(result);
            })
        });
    </script>
</div>
