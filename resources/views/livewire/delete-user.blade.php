<div>
    @if (session()->has('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
    @endif
    <x-jet-confirmation-modal wire:model="showModal">
        <x-slot name="title">
            Eliminar usuario
        </x-slot>
        <x-slot name="content">
            @if (!empty($user) )
                ¿Estás seguro que deseas eliminar al usuario {{ $user->name }} ?    
            @endif
        </x-slot>
        <x-slot name="footer">
            <x-jet-danger-button wire:click="deleteUser">Sí, eliminar</x-jet-danger-button>
            <x-jet-secondary-button wire:click="$toggle('showModal')">Cancelar</x-jet-secondary-button>
        </x-slot>
    </x-jet-confirmation-modal>
</div>
