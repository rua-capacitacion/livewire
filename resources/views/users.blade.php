<x-guest-layout class="bg-emerald-500">
    <h1 class="m-auto">Administración de usuarios</h1>
    <livewire:formulario />
    <livewire:lista-usuarios />
    <livewire:delete-user />
</x-guest-layout>
