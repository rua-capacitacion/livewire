<?php

namespace App\Traits;

trait WithSorting {

    public $sortBy = 'id';
    public $sortDir = 'asc';

    public function sortBy($field) {
        
        $this->sortDir = $field == $this->sortBy ?
            $this->reverseSort()
            : 'asc';
        
        $this->sortBy = $field;
    }

    private function reverseSort(){
        return $this->sortDir=='asc'?'desc':'asc';
    }

}

?>
