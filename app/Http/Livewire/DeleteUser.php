<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;

class DeleteUser extends Component
{
    public $showModal = false;
    public $user = null;

    protected $listeners = ['confirmDeleteUser'];

    public function render()
    {
        return view('livewire.delete-user');
    }

    public function confirmDeleteUser(User $user){
        $this->showModal = true;
        $this->user = $user;
    }

    public function deleteUser(){
        User::destroy($this->user->id);
        $this->clear();
        session()->flash('message', 'Se eliminó el usuario correctamente');
        $this->emit('userDeleted');
    }

    public function clear(){
        $this->user = null;
        $this->showModal = false;
    }

}
