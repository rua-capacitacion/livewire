<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;
use Illuminate\Support\Facades\Hash;

class Formulario extends Component
{
    public $userId = -1;
    public $name;
    public $email;
    public $password;

    protected $rules = [
        'name' => 'required|min:6',
        'email' => 'required|email',
        //'password' => 'required|min:8',
    ];

    protected $messages = [
        'name.required' => 'El nombre es requerido.',
        'name.min' => 'El nombre debe ser mayor a :min caracteres',
        'email.required' => 'El correo es requerido.',
        'email.email' => 'Formato de correo inválido.',
        'password.required' => 'La contraseña es requerida.',
        'password.min' => 'La contraseña debe ser mayor a :min caracteres',
    ];

    protected $listeners = [
        'editUser' => 'editUser'
    ];


    public function render()
    {
        return view('livewire.formulario')
            ->layout('layouts.guest');
    }

    public function limpiar(){
        $this->name = '';
        $this->email = '';
        $this->password = '';
        $this->userId = -1;

        $this->resetValidation();

    }

    public function submit(){
        
        if($this->userId < 0){
            $this->rules['password'] = 'required|min:8';
        }
        
        $this->validate();

        //Save or update
        User::updateOrCreate(
            [ 'id' => $this->userId ],
            [
                'name' => $this->name,
                'email' => $this->email,
                'password' => Hash::make($this->password)
            ]
        );

        $this->limpiar();
        $this->emit('userCreated');
        session()->flash('message', 'Se registraron los cambios correctamente');
    }

    public function editUser(User $user){

        $this->limpiar();

        $this->userId = $user->id;
        $this->name = $user->name;
        $this->email = $user->email;
    }
}
