<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;
use App\Traits\WithSorting;
use Livewire\WithPagination;

class ListaUsuarios extends Component
{
    use WithSorting;
    use WithPagination;

    public $search;

    protected $listeners = [
        'userCreated' => 'render',
        'userDeleted' => 'render',
    ];

    public function mount(){
        $this->sortBy = 'id';
    }

    public function render()
    {
        return view('livewire.lista-usuarios', [
            'users' => User::query()
                ->where('name', 'LIKE', "%{$this->search}%") 
                ->orderBy($this->sortBy, $this->sortDir)
                ->paginate(3)
        ])->layout('layouts.guest');
    }

}
